# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141201012319) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "job_run", primary_key: "job_run_id", force: true do |t|
    t.integer  "task_id"
    t.datetime "start_time"
    t.datetime "end_time"
    t.string   "status",     limit: 64
  end

  create_table "players", force: true do |t|
    t.string   "name"
    t.boolean  "twitter"
    t.boolean  "facebook"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tasks", force: true do |t|
    t.string   "name"
    t.string   "frequency"
    t.integer  "month",        default: 0
    t.integer  "day_of_week",  default: 0
    t.integer  "day_of_month", default: 0
    t.integer  "hour",         default: 0
    t.string   "command"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "folder"
  end

end
